:sd_hide_title:

====================
Salt success stories
====================


.. toctree::
   :maxdepth: 2
   :hidden:

   topics/overview


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contribute

   See a problem? Open an issue! <https://gitlab.com/saltstack/open/docs/salt-success-stories/-/issues>
   Salt docs contributing guide <https://saltstack.gitlab.io/open/docs/docs-hub/topics/contributing.html>
   GitLab repository <https://gitlab.com/saltstack/open/docs/salt-success-stories>
